package sample;


import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

import java.math.BigDecimal;


public class Controller {

    @FXML
    ImageView map;

    @FXML
    Label cityName;

    @FXML
    TextArea mapOutput;

    @FXML
    ImageView cityImg;


    PlacesList guessingCities = new PlacesList();
    public double cityX;
    public double cityY;
    public int attempCounter = 0;

    public void newGame() {
        attempCounter = 0;
        mapOutput.setText("Pokračujte kliknutím na mapu, kde se nachází město: " + System.lineSeparator() + System.lineSeparator());
        doCity();
        map.setDisable(false);
    }

    public void exit() {
        javafx.application.Platform.exit();
    }


    public static double distance(double x1, double y1, double x2, double y2) {
        double dx = x1 - x2;
        double dy = y1 - y2;
        return Math.sqrt(dx * dx + dy * dy);
    }

    public void doCity() {
        guessingCities.returnCity();
        cityName.setText("Uhádni,kde je: " + guessingCities.eachCity);
        String coordinates = guessingCities.city.get(guessingCities.eachCity);

        String[] xy;
        xy = coordinates.split("/");
        String x = xy[0];
        String y = xy[1];
        cityX = Double.parseDouble(x);
        cityY = Double.parseDouble(y);


        String cityHelpVar = guessingCities.eachCity;

        switch (cityHelpVar) {
            case "Praha":
                cityImg.setImage(new Image("https://www.officehouse.cz/wp-content/uploads/2016/03/praha.jpeg"));
                break;
            case "Brno":
                cityImg.setImage(new Image("https://i.iinfo.cz/images/212/brno-1-prev.jpg"));
                break;
            case "České Budějovice":
                cityImg.setImage(new Image("http://www.starsreality.cz/media/uploads/files_widget/pobocky-realitnich-kancelari/43/ceske%20budejovice.jpg"));
                break;
            case "Karlovy Vary":
                cityImg.setImage(new Image("https://www.czechinvest.org/getattachment/ec76c006-9642-4cf3-8f20-95874c2fb76a/Karlovy-Vary-Regional-Office?maxsidesize=1200"));
                break;
            case "Ostrava":
                cityImg.setImage(new Image("https://s-ec.bstatic.com/images/hotel/max1024x768/139/139913925.jpg"));
                break;
            case "Plzeň":
                cityImg.setImage(new Image("https://www.plzen.eu/Files/MestoPlzen/web2013/Turista/pamatky_turisticke_cile/top_turisticke_cile/160226_uvodni_foto.jpg"));
                break;
            case "Liberec":
                cityImg.setImage(new Image("https://www.estav.cz/img/_/6352/fotolia_7629635_subscription_monthly_m_frank.jpg"));
                break;
            case "Ústí nad Labem":
                cityImg.setImage(new Image("https://media.novinky.cz/588/225883-top_foto1-721bc.jpg?1357300020"));
                break;
            case "Pardubice":
                cityImg.setImage(new Image("https://img.blesk.cz/img/1/normal620/2580950-img-pardubice-namesti-v0.jpg?v=0"));
                break;
            case "Olomouc":
                cityImg.setImage(new Image("https://img.cncenter.cz/img/12/normal1050/4885219_olomouc-ilustracni-namesti-mesto-cesko-v0.jpg?v=0"));
                break;
            case "Český Krumlov":
                cityImg.setImage(new Image("https://www.zamek-ceskykrumlov.cz/website/var/tmp/image-thumbnails/30000/30274/thumb__OgTagImage/panoramaticke-foto-zamek-cesky-krumlov_1.png"));
                break;
        }

    }

    public void inicializuj() {

        mapOutput.setText("Pokračujte kliknutím na mapu, kde se nachází město: " + System.lineSeparator() + System.lineSeparator());

        map.setCursor(Cursor.CROSSHAIR);

        doCity();

        map.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {

                attempCounter++;

                if (attempCounter <= 10) {

                    event.consume();

                    double distance = distance(event.getX(), event.getY(), cityX, cityY);

                    BigDecimal pattern = new BigDecimal(distance);

                    BigDecimal roundedDistance = pattern.setScale(2, BigDecimal.ROUND_DOWN);


                    String outPuttext = "hádal  jsi město " + guessingCities.eachCity + " a netrefil ses o " + roundedDistance + " km" + System.lineSeparator();

                    mapOutput.appendText(outPuttext);

                    doCity();

                } else {
                    cityName.setText("Konec hry");
                    mapOutput.appendText("Konec hry");
                    map.setDisable(true);
                }
            }
        });

    }

}
