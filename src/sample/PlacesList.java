package sample;


import java.util.*;

public class PlacesList {

    private Map<String,String> cities;
    private String label;
    public String eachCity;
    public Map<String,String> city;
    public List<String> keys;

    public PlacesList() {

        this.cities = new HashMap<String, String>();

        cities.put("Praha","190.0/130.0");
        cities.put("České Budějovice","180.0/250.0");
        cities.put("Karlovy Vary","62.0/108.0");
        cities.put("Ostrava","467.0/155.0");
        cities.put("Brno","355.0/240.0");
        cities.put("Plzeň","100.0/170.0");
        cities.put("Liberec","232.0/58.0");
        cities.put("Ústí nad Labem","158.0/65.0");
        cities.put("Pardubice","277.0/132.0");
        cities.put("Olomouc","390.0/185.0");
        cities.put("Český Krumlov","160.0/280.0");
    }

    public Map<String,String> returnCity()
    {
        city = new HashMap<String, String>();
        city = this.cities;
        List<String> keys = new ArrayList<String>(cities.keySet());
        Random random = new Random();
        eachCity = keys.get(random.nextInt(keys.size()-1));
        city.put(eachCity, cities.get(eachCity));
        return city;
    }


}
